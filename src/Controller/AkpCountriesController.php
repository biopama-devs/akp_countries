<?php
namespace Drupal\akp_countries\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides route responses for the AKP country page.
 */
class AkpCountriesController extends ControllerBase {
  public function country($iso_code = null) {
    $element = array(
	  '#theme' => 'page_akp_country',
      '#iso_code' => $iso_code,
    );
    return $element;
  }

  public function reportCountry() {
    $element = array(
	  '#theme' => 'page_reporting',
    );
    return $element;
  }
}
