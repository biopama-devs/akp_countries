/*   
## Global Page Variables ##
so we can set them in the js that will be called later without passing them.
*/
var DopaBaseUrlOld = "https://dopa-services.jrc.ec.europa.eu/services/d6dopa40/";
var DopaBaseUrl41 = "https://dopa-services.jrc.ec.europa.eu/services/d6dopa/dopa_41/";
var DopaBaseUrl = "https://dopa-services.jrc.ec.europa.eu/services/d6dopa/dopa_42/";
var dopaAttribution = "Data Source: DOPA (https://dopa-explorer.jrc.ec.europa.eu/)";
var DOPAgetCountryExtent = "https://rest-services.jrc.ec.europa.eu/services/d6biopamarest/administrative_units/get_country_extent_by_iso?format=json&a_iso=";
var DOPAgetWdpaExtent = "https://rest-services.jrc.ec.europa.eu/services/d6biopamarest/d6biopama/get_wdpa_extent?format=json&wdpa_id=";
var imagePath =  drupalSettings.dopaImgPath;
var mymap; 

var selSettings = {
    paName: 'default',
	WDPAID: 0,
    countryName: 'trans-ACP',
    regionID: null,
	regionName: null,
	iso2: null,
	iso3: null,
	num: null,
};

var miniLoadingSpinner = "<div id='mini-loader-wrapper'><div id='mini-loader'></div></div>";
var activeLayers = {}; //To track what's in the map and maybe do something cool with it.
var activeIndicators = []; //To track the indicators and get them in the report
var reportIndicators = {}; 

/*   
## Page ##
*/
(function ($, Drupal) {
	
 	//$('[data-bs-toggle="tooltip"]').tooltip(); 
	
	var currentWindowHeight = $().getWindowHeight(); //this is needed every time the map is resized (on window resize too)
	var currentWindowHeight = currentWindowHeight + 29; //add 20 temporarily due to differences in the navbar style. 
	console.log(drupalSettings.dataCards);
	// mymap = $().createMap('container-mapbox-map');
	// class mapExpandControl {
	// 	onAdd(map) {
	// 		this._map = map;
	// 		this._container = document.createElement('div');
	// 		this._container.className = 'mapboxgl-ctrl mapbox-expand-map';
	// 		this._container.innerHTML = '<button type="button" class="btn btn-expand-map btn-outline-success rounded-0"><i class="fas fa-chevron-left"></i></button>';
	// 		return this._container;
	// 	}
	// 	onRemove() {
	// 		this._container.parentNode.removeChild(this._container);
	// 		this._map = undefined;
	// 	} 
	// }
	// class mapLoadingSpinnerControl {
	// 	onAdd(map) {
	// 		this._map = map;
	// 		this._container = document.createElement('div');
	// 		this._container.className = 'mapbox-loading-spinner invisible';
	// 		this._container.innerHTML = miniLoadingSpinner;
	// 		return this._container;
	// 	}
	// 	onRemove() {
	// 		this._container.parentNode.removeChild(this._container);
	// 		this._map = undefined;
	// 	} 
	// }
	// var mapExpand = new mapExpandControl();
	// mymap.addControl(mapExpand, 'top-left');	
	// var mapLoadingSpinner = new mapLoadingSpinnerControl();
	// mymap.addControl(mapLoadingSpinner, 'top-left');

	// $().addMapControls(mymap, "fullScreen");
	// $().addMapControls(mymap, "navigation");	
	
	// $('.btn-expand-map').bind("click", function(){
	// 	$("#container-data").toggleClass('collapse-column');
	// 	$(this).toggleClass('collapseActivated');
	// 	var mapResizing = setInterval(startResizeingMap, 10); //refresh every 10ms 
		
	// 	function startResizeingMap(){
	// 		resizeMap();
	// 	}
	// 	setTimeout(stopResizeingMap, 1020);//stop after 1020ms !important as in the css the transition animation is 1000ms
	// 	function stopResizeingMap(){
	// 		clearInterval(mapResizing);
	// 	}
	// });
	
	// $(window).resize(function(){
	// 	resizeMap();
	// });
	

	// function resizeMap(){
	// 	mymap.resize();
	// } 

	// resizeMap();

	// $().addMapLayerBiopamaSources(mymap);
	// $().addMapControls(mymap, "satelliteToggle");
		
	// $().addMapLayer(mymap, "biopamaGaulEez");
	// $().addMapLayer(mymap, "biopamaRegions");
	// $().addMapLayer(mymap, "biopamaCountries");
	// $().addMapLayer(mymap, "biopamaWDPAPolyJRC");
	// $().addMapLayer(mymap, "biopamaWDPAPoint");
	// $().addMapLayer(mymap, "CountriesRedGreen");
    // $().addMapLayer(mymap, "CountriesGoodBad");
	// $().addMapLayer(mymap, "satellite");
	// $().addMapLayer(mymap, "nan-layers");	

	// mymap.on('load', function () {

	// 	//mymap.setLayoutProperty("world_mask", 'visibility', 'visible');
	// 	mymap.addSource('world_mask', {
	// 	'type': 'geojson',
	// 	'data': {
	// 	'type': 'Feature',
	// 	'geometry': {
	// 	'type': 'Polygon',
	// 		"coordinates": [
	// 		  [
	// 			[-180,-90],[180,-90],[180,90],[-180,90],[-180,-90]
	// 		  ]
	// 		]
	// 	}
	// 	}
	// 	});
	// 	mymap.addLayer({
	// 		'id': 'world_mask',
	// 		'type': 'fill',
	// 		'source': 'world_mask',
	// 		'layout': { 'visibility': 'none'},
	// 		'paint': {
	// 		'fill-color': '#000',
	// 		'fill-opacity': 0.7
	// 		}
	// 	}, '');
		
	// });
	// mymap.on('sourcedata', function(data) {
	//   	if (data.isSourceLoaded == false){
	// 		$(".mapbox-loading-spinner").removeClass("invisible");
	// 		$(".mapbox-loading-spinner").addClass("visible");
	//   	} else {
	// 	 	$(".mapbox-loading-spinner").removeClass("visible");
	// 		$(".mapbox-loading-spinner").addClass("invisible");
	//   	}
	// });
	$( "#report-link" ).click(function( event ) {
		$().goToIndicatorReport(); 
	});
	
})(jQuery, Drupal);
