(function ($, Drupal) {


    $(".ctt-card-title").on( 'click', function(e) {
        if ($(e.currentTarget).hasClass('collapsed')){
            //closeIndicatorCard();
        } else {
            var nodeID = $(this).next().find("div.ctt-nid").text().trim();
            currentIndicatorNodeURL = '/indicator_card/'+nodeID;
            showIndicatorCard(currentIndicatorNodeURL, nodeID);
        }
    });

    function showIndicatorCard(indicatorURL, nodeID){
        //$('.wrapper-ctt-data-card > div#view-wrapper-'+nodeID).empty();//acctually delete the indicator card as it will break the detection of the other CTT indicator variables if it stays.
        // Drupal.ajax({ 
        //     url: indicatorURL,
        //     success: function(response) {			
        //         var $countryDialogContents
        //         for (var key in response) {
        //             // skip loop if the property is from prototype
        //             if (!response.hasOwnProperty(key)) continue;
        //             var obj = response[key];
        //             for (var prop in obj) {
        //                 // skip loop if the property is from prototype
        //                 if(!obj.hasOwnProperty(prop)) continue;
        //                 //console.log(prop + " = " + obj[prop]);
        //                 if(prop == "data"){
        //                     //console.log(prop + " = " + obj[prop]);
        //                     //$('#block-indicatorcard').show();
        //                     $countryDialogContents = $('<div>' + response[key].data + '</div>').appendTo('body');
        //                 }
        //             }
        //         }
        //         $().removeBiopamaLoader("#view-wrapper-"+nodeID); 
        //         $("#view-wrapper-"+nodeID).empty();
        //         $countryDialogContents.appendTo("#view-wrapper-"+nodeID);
        //         Drupal.attachBehaviors($('.wrapper-ctt-data-card > div.ctt-wrapper').get(0));
        //     }
        // }).execute(); 
        var nodeDatasetCard = document.getElementById("id-"+nodeID);
        if (nodeDatasetCard === null){
            $().insertBiopamaLoader("#view-wrapper-"+nodeID); 
            var ajaxSettings = {
                url: indicatorURL,
                wrapper: "view-wrapper-"+nodeID,
                method: 'append',
            };
            var myAjaxObject = Drupal.ajax(ajaxSettings);
            myAjaxObject.execute();
            Drupal.attachBehaviors($('.wrapper-ctt-data-card > div#view-wrapper-'+nodeID).get(0));
        } else {
            $().removeBiopamaLoader("#view-wrapper-"+nodeID); 
        }
    }

    // let  lazyOptions = {
    //     root: null, // Use the viewport as the root
    //     rootMargin: '0px' // Specify the threshold for intersection
    // };

    // const lazyCards = document.querySelectorAll(".ctt-card-title");

    // const handleIntersection = (entries, observer) => {
    //     entries.forEach((entry) => {
    //       if (entry.isIntersecting) {
    //         const card = entry.target;
    //         card.click();
    //         // Stop observing the card
    //         observer.unobserve(card);
    //       }
    //     });
    // };

    // const observer = new IntersectionObserver(handleIntersection, lazyOptions);

    // lazyCards.forEach((card) => {
    //     observer.observe(card);
    // });

})(jQuery, Drupal);